import {Component, OnInit} from '@angular/core'
import {PokemonService} from "../services/fetchPokemons.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})

export class TrainerPageComponent implements OnInit {
  constructor(private readonly fetchPoke: PokemonService,
              private router: Router) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('User') === null) {
      this.router.navigate(['/landing']);
    }
  }
  //returning string of pokemons
  get localPokemons(): any {
    return JSON.parse(JSON.stringify([localStorage.getItem('userPokemons')]));
  }
}
