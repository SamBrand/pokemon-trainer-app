import { NgModule } from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LandingPage} from "./landing/landing.page";
import {TrainerPageComponent} from "./trainer-page/trainer-page.component";
import {CataloguePage} from "./pokemon-catalogue-page/catalogue.page";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing'
  },
  {
    path: 'landing',
    component: LandingPage
  },
  {
    path: 'catalogue',
    component: CataloguePage
  },
  {
    path: 'landing/trainer',
    component: TrainerPageComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
