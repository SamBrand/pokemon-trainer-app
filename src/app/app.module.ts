import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./app-routing.module";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

import { AppComponent } from './app.component';
import {PokemonSelectedComponent} from "./pokemon-selected/pokemon-selected.component";
import {TrainerPageComponent} from "./trainer-page/trainer-page.component";
import {LandingPage} from "./landing/landing.page";
import {CataloguePage} from "./pokemon-catalogue-page/catalogue.page";



@NgModule({
  declarations: [
    AppComponent,
    TrainerPageComponent,
    LandingPage,
    CataloguePage,
    PokemonSelectedComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
