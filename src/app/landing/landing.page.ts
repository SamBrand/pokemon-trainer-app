import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Users} from "../models/users.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css']
})
export class LandingPage{
  constructor(private router: Router) {
  }

  private _users: Users[] = [];

  public onLogin(loginForm: NgForm): void {
    const trainerName = JSON.parse(JSON.stringify(loginForm.value.username));
    localStorage.setItem('User', trainerName);
    console.log(localStorage.getItem('User'))

    this.router.navigate(['/catalogue']);
  }
}
