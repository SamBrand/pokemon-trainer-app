import {Component, OnInit} from "@angular/core";
import {PokemonService} from "../services/fetchPokemons.service";
import {Pokemon} from "../models/pokemon.model";
import {SelectedPokemonService} from "../services/selected-pokemon.service";


@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})

export class CataloguePage implements OnInit {
  constructor(
    private readonly fetchPoke: PokemonService,
    private readonly selectedPokemon: SelectedPokemonService) {
  }
  private collectedPokemons: string[] = [];
  ngOnInit(): void {
    this.fetchPoke.fetchPokemons();
  }
  get pokemons(): Pokemon[] {
    return this.fetchPoke.pokemons();
  }
  onPokemonClicked(pokemon: Pokemon): void {
    this.selectedPokemon.setPokemon(pokemon);
    this.collectedPokemons.push(pokemon.name);
    const pokemonList = JSON.parse(JSON.stringify(this.collectedPokemons))
    localStorage.setItem('userPokemons', pokemonList)

  }
  public getCollectedPokemons(): string[] {
    return this.collectedPokemons;
  }
}
