import { Injectable } from "@angular/core";
import {Pokemon} from "../models/pokemon.model";

@Injectable({
  providedIn: 'root'
})
export class SelectedPokemonService {
  private _pokemon: Pokemon | null = null;

  public setPokemon(poke: Pokemon) {
    this._pokemon = poke;
  }
  public get pokemon(): Pokemon | null {

    return this._pokemon;
  }
}
