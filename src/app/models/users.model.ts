import {Pokemon} from "./pokemon.model";

export interface Users {
  id: number;
  name: string;
  pokeList: Pokemon[];
}
